package com.sample;

import org.apache.http.client.utils.URIBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class ApiSource {

    private static final String API_KEY = "b7cb01b5bc2ebd870f6b32f64cd482bb";

    private static final String serverHost = "api.megaindex.com";
    private static final String serverScheme = "http";

    private static final String getTextSubjects = "/visrep/lda_content";

    public String getTextSubjects(String text) throws InterruptedException, IOException, URISyntaxException {
        // Api call
        URI serverUrl = new URIBuilder()
                .setScheme(serverScheme)
                .setHost(serverHost)
                .setPath(getTextSubjects)
                .setParameter("key", API_KEY)
                .setParameter("content", text)
                .build();

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = java.net.http.HttpRequest.newBuilder()
                .uri(serverUrl)
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        return jsonParser(new JSONObject(response.body()));
    }

    // Парсинг результата
    private String jsonParser(JSONObject jsonObject) throws JSONException {
        String resultText = "";

        JSONArray mainArray = jsonObject.getJSONArray("data");
        JSONObject arrayObject = mainArray.getJSONObject(0);
        JSONArray arrayRezult = arrayObject.getJSONArray("topics");
        for (int i = 0; i < arrayRezult.length(); i++) {
            JSONObject object = arrayRezult.getJSONObject(i);
            String theme = object.getString("n");
            Double values = object.getDouble("w");
            resultText += (i + 1) + ". " + theme + ": " + values + "%" + "\n";
        }
        return resultText;
    }
}
